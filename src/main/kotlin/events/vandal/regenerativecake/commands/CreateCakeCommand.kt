package events.vandal.regenerativecake.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.regenerativecake.database.Cake
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.Particle
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.transactions.transaction

object CreateCakeCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val location = args[0] as Location
        val delay = args[1] as Int
        val food = args[2] as Int
        val sat = args[3] as Int

        println("Creating cake at $location with delay of $delay")

        val cake = transaction {
            return@transaction Cake.new {
                locationX = location.blockX
                locationY = location.blockY
                locationZ = location.blockZ
                regenDelay = delay
                slicesEaten = 0
                foodLevel = food
                saturation = sat
                world = sender.world.name
            }
        }

        sender.spawnParticle(Particle.HEART, location.clone().add(0.5, 0.5, 0.5), 1)
        location.block.type = Material.CAKE

        sender.sendMessage(
            ChatColor.translateAlternateColorCodes('&', "&8[&a+&8] &7Created regenerative cake with ID &b${cake.id} &7with regen delay of &e$delay&7, food of &e$food &7and saturation of &e$sat &7at &9${location.blockX},${location.blockY},${location.blockZ}")
        )
    }
}