package events.vandal.regenerativecake

import events.vandal.regenerativecake.commands.DestroyCakeCommand
import events.vandal.regenerativecake.database.Cake
import events.vandal.regenerativecake.database.Cakes
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.scheduler.BukkitRunnable
import org.jetbrains.exposed.sql.transactions.transaction

class CakeListener : Listener {
    @EventHandler
    fun cakeInteractionListener(e: PlayerInteractEvent) {
        val player = e.player
        if (e.action == Action.LEFT_CLICK_BLOCK && e.clickedBlock !== null) {
            if (player.hasPermission("regenerativecake.admin")) {
                if (e.clickedBlock!!.type == Material.CAKE) {
                    DestroyCakeCommand.run(player, arrayOf(e.clickedBlock!!.location))
                }
            }
        }
        if (e.action == Action.RIGHT_CLICK_BLOCK && e.clickedBlock !== null) {
            if (e.clickedBlock!!.type == Material.CAKE) {
                transaction {
                    val cake = Cake.find {
                        Cakes.locationX eq e.clickedBlock!!.location.blockX
                        Cakes.locationY eq e.clickedBlock!!.location.blockY
                        Cakes.locationZ eq e.clickedBlock!!.location.blockZ
                    }.firstOrNull()
                    if (cake !== null) {
                        player.foodLevel = cake.foodLevel
                        player.saturation = cake.saturation.toFloat()

                        object : BukkitRunnable() {
                            override fun run() {
                                RegenerativeCake.plugin.server.getWorld(cake.world)?.getBlockAt(cake.locationX, cake.locationY, cake.locationZ)?.type = Material.CAKE
                            }

                        }.runTaskLater(RegenerativeCake.plugin, cake.regenDelay.toLong() * 20)
                    }
                }
            }
        }
    }
}