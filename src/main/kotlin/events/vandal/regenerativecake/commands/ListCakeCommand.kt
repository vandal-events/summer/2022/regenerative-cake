package events.vandal.regenerativecake.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.regenerativecake.database.Cake
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.transactions.transaction

object ListCakeCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val messages = transaction {
            return@transaction Cake.all().map {
                return@map ChatColor.translateAlternateColorCodes(
                    '&',
                    "&8[&b${it.id}&8] &8(&9${it.locationX}, ${it.locationY}, ${it.locationZ}&8) &7delay: &e${it.regenDelay}&7, food: &e${it.foodLevel}&7, saturation: &e${it.saturation} &7in &e${it.world}"
                )
            }.ifEmpty {
                listOf(ChatColor.translateAlternateColorCodes(
                    '&',
                    "&8[&c?&8] &cNo cakes exist."
                ))
            }
        }

        sender.sendMessage(messages.joinToString("\n"))
    }
}