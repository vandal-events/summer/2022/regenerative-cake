package events.vandal.regenerativecake.database

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Cakes : IntIdTable() {
    val world: Column<String> = varchar("world", 100)
    val regenDelay: Column<Int> = integer("regen_delay")
    val slicesEaten: Column<Int> = integer("slices_eaten")
    val foodLevel: Column<Int> = integer("food_level")
    val saturation: Column<Int> = integer("saturation")
    val locationX: Column<Int> = integer("location_x")
    val locationY: Column<Int> = integer("location_y")
    val locationZ: Column<Int> = integer("location_z")
}

class Cake(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Cake>(Cakes)
    var world by Cakes.world
    var regenDelay by Cakes.regenDelay
    var slicesEaten by Cakes.slicesEaten
    var foodLevel by Cakes.foodLevel
    var saturation by Cakes.saturation
    var locationX by Cakes.locationX
    var locationY by Cakes.locationY
    var locationZ by Cakes.locationZ
}