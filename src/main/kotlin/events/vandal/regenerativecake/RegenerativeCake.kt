package events.vandal.regenerativecake

import dev.jorel.commandapi.CommandAPI
import dev.jorel.commandapi.CommandAPICommand
import dev.jorel.commandapi.CommandAPIConfig
import dev.jorel.commandapi.StringTooltip
import dev.jorel.commandapi.arguments.ArgumentSuggestions
import dev.jorel.commandapi.arguments.IntegerArgument
import dev.jorel.commandapi.arguments.LocationArgument
import events.vandal.regenerativecake.commands.CreateCakeCommand
import events.vandal.regenerativecake.commands.DestroyCakeCommand
import events.vandal.regenerativecake.commands.ListCakeCommand
import events.vandal.regenerativecake.database.Cake
import events.vandal.regenerativecake.database.Cakes
import org.bukkit.Material
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction

class RegenerativeCake : JavaPlugin() {
    override fun onLoad() {
        CommandAPI.onLoad(CommandAPIConfig())
    }
    override fun onEnable() {
        plugin = this
        RegenerativeCake.config = this.config

        config.addDefault("delay", 1)
        config.addDefault("foodLevel", 20)
        config.addDefault("saturation", 20)
        config.options().copyDefaults(true)
        saveConfig()

        //metrics = MetricManager(CakeMetricData::class, this.dataFolder.apply { this.mkdirs() }.toPath(), "Summer 2022")
        Database.connect("jdbc:h2:./${this.dataFolder.apply { this.mkdirs() }.toPath()}/data;DB_CLOSE_DELAY=-1;", "org.h2.Driver")

        CommandAPI.onEnable(this)

        CommandAPICommand("cake").apply {
            withPermission("regenerativecake.admin")
            withRequirement { it is Player }

            withSubcommand(CommandAPICommand("list").apply {
                executesPlayer(ListCakeCommand)
            })

            withSubcommand(CommandAPICommand("create").apply {
                withArguments(
                    LocationArgument("position"),
                    IntegerArgument("delay").replaceSuggestions(
                        ArgumentSuggestions
                            .stringsWithTooltips(StringTooltip.of(config.getInt("delay").toString(), "Regeneration speed of the slice (in seconds)"))
                    ),
                    IntegerArgument("foodLevel").replaceSuggestions(
                        ArgumentSuggestions
                            .stringsWithTooltips(StringTooltip.of(config.getInt("foodLevel").toString(), "How much food points to restore?"))
                    ),
                    IntegerArgument("saturation").replaceSuggestions(
                        ArgumentSuggestions
                            .stringsWithTooltips(StringTooltip.of(config.getInt("saturation").toString(), "The amount of saturation to apply?"))
                    ),
                )

                executesPlayer(CreateCakeCommand)
            })

            withSubcommand(CommandAPICommand("destroy").apply {
                withArguments(
                    IntegerArgument("id")
                )

                executesPlayer(DestroyCakeCommand)
            })

            withSubcommand(CommandAPICommand("destroy").apply {
                withArguments(
                    LocationArgument("position")
                )

                executesPlayer(DestroyCakeCommand)
            })
        }.register()

        transaction {
            SchemaUtils.createMissingTablesAndColumns(
                Cakes
            )
        }

        transaction {
            Cake.all().forEach {
                server.getWorld(it.world)?.getBlockAt(it.locationX, it.locationY, it.locationZ)?.type = Material.CAKE
            }
        }

        server.pluginManager.registerEvents(CakeListener(), this)
    }

    override fun onDisable() {
        TransactionManager.currentOrNull()?.connection?.close()
        TransactionManager.resetCurrent(null)
        CommandAPI.unregister("cake")
        //metrics.save()
    }

    companion object {
        lateinit var plugin: JavaPlugin
        lateinit var config: FileConfiguration
        //lateinit var metrics: MetricManager<CakeMetricData>
    }
}