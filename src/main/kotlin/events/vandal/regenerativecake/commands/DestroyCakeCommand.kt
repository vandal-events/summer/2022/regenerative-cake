package events.vandal.regenerativecake.commands

import dev.jorel.commandapi.executors.PlayerCommandExecutor
import events.vandal.regenerativecake.database.Cake
import events.vandal.regenerativecake.database.Cakes
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.Particle
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

object DestroyCakeCommand : PlayerCommandExecutor {
    override fun run(sender: Player, args: Array<out Any>) {
        val identifier = args[0]
        val cake = when (identifier) {
            is Int -> transaction {
                return@transaction Cake.findById(identifier).let {
                    if (it == null) {
                        sender.sendMessage(
                            ChatColor.translateAlternateColorCodes(
                                '&',
                                "&8[&3!&8] &cNo cake with the ID &b${identifier} &cexists."
                            )
                        )
                        return@let null
                    } else {
                        return@let it
                    }
                }
            }

            is Location -> transaction {
                return@transaction Cake.find {
                    (Cakes.locationX eq identifier.blockX) and
                    (Cakes.locationY eq identifier.blockY) and
                    (Cakes.locationZ eq identifier.blockZ)
                }.firstOrNull().let {
                    if (it == null) {
                        sender.sendMessage(
                            ChatColor.translateAlternateColorCodes(
                                '&',
                                "&8[&3!&8] &cNo cake at &9${identifier.blockX},${identifier.blockY},${identifier.blockZ} &cwas found in the list, try removing it by it's &bID"
                            )
                        )
                        return@let null
                    } else {
                        return@let it
                    }
                }
            }

            else -> null
        }

        transaction {
            if (cake == null) return@transaction

            val block = sender.server.getWorld(cake.world)?.getBlockAt(cake.locationX, cake.locationY, cake.locationZ) ?: return@transaction sender.sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&8[&3!&8] &cCan't find the cake in the world, perhaps it has already been removed? place a cake and try the command again.")
            )

            cake.delete()
            block.breakNaturally(true)
            block.world.spawnParticle(Particle.EXPLOSION_NORMAL, block.location.clone().add(0.5, 0.5, 0.5), 3)

            val format = when (identifier) {
                is Location -> "${identifier.blockX},${identifier.blockY},${identifier.blockZ}"
                else -> identifier
            }

            sender.sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&8[&c-&8] &7Cake &b${format} &7has been removed!")
            )
        }
    }
}