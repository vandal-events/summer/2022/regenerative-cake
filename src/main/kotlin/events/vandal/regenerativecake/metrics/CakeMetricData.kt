package events.vandal.regenerativecake.metrics

import com.google.gson.JsonObject
import com.mojang.authlib.GameProfile
import events.vandal.metrics.MetricData
import java.util.UUID

data class CakeMetricData(
    val player: GameProfile,
    val cakesEaten: MutableMap<UUID, Int> = mutableMapOf() // UUID is the cake UUID
) : MetricData(player) {
    override fun serialize(): JsonObject {
        return super.serialize()
    }

    companion object {
        fun deserialize(data: JsonObject): CakeMetricData {
            val profile = MetricData.deserialize(data)

            return CakeMetricData(
                profile
            )
        }
    }
}