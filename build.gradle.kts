plugins {
    java
    kotlin("jvm") version "1.7.21"
    id("com.github.johnrengelman.shadow") version "7.1.0"
    id("xyz.jpenilla.run-paper") version "1.0.6"
}

group = "vandal.events"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://papermc.io/repo/repository/maven-public/")
    maven("https://oss.sonatype.org/content/groups/public/")
    maven("https://gitlab.com/api/v4/projects/35286020/packages/maven")
    maven("https://jitpack.io")
}

val minecraftGameVersion: String by project
val paperVersion: String by project
val exposedVersion: String by project
dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")

    compileOnly("io.papermc.paper:paper-api:$minecraftGameVersion-$paperVersion")
    compileOnly("com.mojang:authlib:3.11.50")

    implementation("events.vandal:VandalMetrics:1.0.0")

    implementation("dev.jorel:commandapi-shade:8.5.1")

    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("com.h2database:h2:2.1.214")
}

tasks {
    runServer {
        minecraftVersion(minecraftGameVersion)
    }

    processResources {
        val props = mapOf("version" to project.version)
        inputs.properties(props)
        filteringCharset = "UTF-8"
        filesMatching("plugin.yml") {
            expand(props)
        }
    }

    shadowJar {
        mergeServiceFiles()

        //relocate("org.jetbrains", "shadow.vandal.regenerativecake.jetbrains")
        relocate("dev.jorel", "shadow.vandal.regenerativecake.jorel")
        //relocate("kotlin", "shadow.vandal.regenerativecake.kotlin")
        //relocate("kotlinx", "shadow.vandal.regenerativecake.kotlinx")
    }
}